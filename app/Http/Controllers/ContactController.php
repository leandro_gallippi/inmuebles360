<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param integer $id
     * @return Response
     */


    public function get($id)
    {
        $user = DB::table('users')->where('id', $id)->first();
        return view('property.contact', ['title' => 'Contact','page' => 'contact', 'vendedor' => $user]
        );
    }


}

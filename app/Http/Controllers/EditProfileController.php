<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class EditProfileController extends Controller
{
    
    public function index()
    {
        return view('profile.editProfile', ['title' => 'Editar Perfil', 'page' => 'perfil']);
    }

    public function edit(Request $request)
    {   
    	$user = app()->make('currentUser');
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->phone = $request->input('phone');
        $user->mobile_phone = $request->input('mobile_phone');
        $user->cuit = $request->input('cuit');
        if($user->password != $request->input('password')) {
            $user->password = bcrypt($request->input('password'));
        }
        $user->save();
        return redirect('/profile');
    }

}
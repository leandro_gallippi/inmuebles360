<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use App\Models\Property as Property;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $properties = Property::skip(0)->take(3)->get();

		$photos = array();
 
		foreach($properties as $propiedad)
		{
			$photo = DB::table('photos')->where('property_id',$propiedad->id)->first();
			$photos = array_add($photos,$propiedad->id,$photo->id);
		}

        return view('home.home', ['title' => 'Home',
                                'page' => 'home',
                                'properties' => $properties,'photos'=>$photos]
        );
    }
}

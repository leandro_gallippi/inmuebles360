<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Property as Property;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    
    public function index()
    {
        return view('profile.profile', ['title' => 'Perfil', 'page' => 'perfil']);
    }

	public function getProperties()
    {
    	$ownProperties = Property::where('user_id', '=', app()->make('currentUser')->id)->get();
        return view('profile.properties', ['title' => 'Mis publicaciones', 'page' => 'mis publicaciones', 'ownProperties' => $ownProperties]);
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param integer $id
     * @return Response
     */


    public function getView($id)
    {
        return view('property.view360', ['title' => 'View 360','page' => 'property','nombreImagen'=>$id]
        );
    }


}

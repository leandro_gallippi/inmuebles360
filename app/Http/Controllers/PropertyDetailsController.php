<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

use App\Models\Property;
use App\Models\Comment;


class PropertyDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param integer $id
     * @return Response
     */


    public function getView($id)
    {
        $property = Property::find($id);
        return view('property.propertyDetails', ['title' => 'View 360',
        										'page' => 'property',
        										'property' => $property]);
    }

	public function comment(Request $request)
	{
		$comment = new Comment();
		$comment->sender_id = 1;
		$comment->sender_name = "";
		$comment->sender_lastname = "";
		$comment->sender_phone = "";
		$comment->sender_mail = "";
		$comment->receiver_id = 1;
		$comment->subject = "";
		$comment->message = $request->message;
		$comment->date_time = date('Y-m-d H:i:s');
		$comment->property_id = $request->property_id;
		$comment->save();
		return view ('property.comment-success', ['title' => 'Publicacion exitosa',
												'page' => 'success',
												'publicationId'=>$request->property_id]);
	}

}

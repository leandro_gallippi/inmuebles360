<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Property;
use App\Models\Photo;

define("PHOTO_PATH", "/home/juan/inmuebles360fotos");

class PublishController extends Controller
{
    
    public function index()
    {
        return view('home.publish', ['title' => 'Home', 'page' => 'home']);
    }

    private function evaluateBoolean($value) {
        if ($value){ 
            return 1;
        }
        return 0;
    }
    
    private function evaluateNullableBoolean($value) 
    {
        return is_null($value) ? false : $this->evaluateBoolean($value);
    }
    
    private function savePhotoOnFileSystem($photo, $name, $path) 
    {
        $photo->move($path, $name);
    }
    
    private function savePhotoArray($photoArray, $propertyId) 
    {
        foreach ($photoArray as $photo){
            if (!is_null($photo)){
                $photoOnDB = $this->createPhoto($propertyId);
                $photoOnDB->save();
                $this->savePhotoOnFileSystem($photo, $photoOnDB->id, PHOTO_PATH);
            } else {
                return;
            }
        }
    }
    
    private function savePhoto360Array($photoArray, $propertyId) 
    {
        foreach ($photoArray as $photo){
            if (!is_null($photo)){
                $photoOnDB = $this->create360Photo($propertyId);
                $photoOnDB->save();
                $this->savePhotoOnFileSystem($photo, $photoOnDB->id, PHOTO_PATH);
            } else {
                return;
            }
        }
    }
    
    private function createGenericPhoto($propertyId){
        $photo = new Photo;
        $photo->property_id = $propertyId;
        $photo->path = PHOTO_PATH;
        return $photo;
    }
    
    private function createPhoto($propertyId){
        $photo = $this->createGenericPhoto($propertyId);
        $photo->photo360 = false;
        return $photo;
    }
    
    private function create360Photo($propertyId){
        $photo = $this->createGenericPhoto($propertyId);
        $photo->photo360 = true;
        return $photo;
    }
        
    private function savePhotos($request, $propertyId) {
        /*if (!is_null($request->photos)){
            $this->savePhotoArray($request->photos, $propertyId);
        }*/
        if (!is_null($request->photos360)){
            $this->savePhoto360Array($request->photos360, $propertyId);
        }
    }

    public function store(Request $request)
    {   
        $property = new Property;
        $property->title = $request->titulo;
        $property->description = $request->desc;
        $property->type = $request->type;
        $property->modality = $request->modality;
        $property->price = $request->precio;
        $property->rooms = $request->ambientes;
        $property->bathrooms = $request->banios;
        $property->total_area = $request->supTotal;
        $property->covered_area = $request->supCubierta;
        $property->plain_water = $this->evaluateNullableBoolean($request->aguaCorriente);
        $property->natural_gas = $this->evaluateNullableBoolean($request->gasNatural);
        $property->light = $this->evaluateNullableBoolean($request->luz);
        $property->pavement = $this->evaluateNullableBoolean($request->pavimentado);
        $property->kitchen = $this->evaluateNullableBoolean($request->cocina);
        $property->living_room = $this->evaluateNullableBoolean($request->livingRoom);
        $property->playground = $this->evaluateNullableBoolean($request->playGround);
        $property->antique = $request->antiguedad;
        $property->formatted_address = $request->formatted_address;
        $property->country = $request->country;
        $property->locality = $request->locality;
        $property->administrative_area_level_1 = $request->administrative_area_level_1;
        $property->administrative_area_level_2 = $request->administrative_area_level_2;
        $property->administrative_area_level_3 = $request->administrative_area_level_3;
        $property->route = $request->route;
        $property->street_number = $request->street_number;
        $property->sublocality_level_1 = $request->sublocality_level_1;
        $property->sublocality_level_2 = $request->sublocality_level_2;
        $property->sublocality_level_3 = $request->sublocality_level_3;
        $property->latitude = $request->latitude;
        $property->longitude = $request->longitude;
        $property->user_id = app()->make('currentUser')->id;

        $property->save();
        $this->savePhotos($request, $property->id);
        return view ('home.publish-success', ['title' => 'Publicacion exitosa', 'page' => 'success', 'publicationId' => $property->id]);
    }
    
    
    

}
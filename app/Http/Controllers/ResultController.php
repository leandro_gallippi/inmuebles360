<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Presenters\Properties\PropertiesResultPresenter;

class ResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $presenter = new PropertiesResultPresenter();

        return view('home.results', ['title' => 'Home', 'page' => 'home', 'presenter' => $presenter]);
    }
}


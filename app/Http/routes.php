<?php

include app_path() . "/ioc.php";
use \Input as Input;
use \Session as Session;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

Route::get('property/{id}', 'PropertyController@getView');
Route::get('/', "HomeController@index");

Route::get('propertyDetails/{id}','PropertyDetailsController@getView');
Route::post('propertyDetails/comment','PropertyDetailsController@comment');

Route::get('contact/{id}','ContactController@get');
Route::get('properties', "ResultController@index");
Route::get('publish', "PublishController@index");
Route::post('publish/store', "PublishController@store");
Route::get('help', "HelpController@index");
Route::get('profile', "ProfileController@index");
Route::get('profile/properties', "ProfileController@getProperties");
Route::get('editProfile', "EditProfileController@index");
Route::post('editProfile', "EditProfileController@edit");

Route::group(array('middleware' => 'auth'), function() {

  Route::post('/login-as', function(){
      if (app()->make('loggedUser')->id == Input::get('id')){
        Session::forget("loginAs");
      }
      else {
        Session::put("loginAs", Input::get('id'));
      }
      return redirect(Input::get('back'));
  });
});

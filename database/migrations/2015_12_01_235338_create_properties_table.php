<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->text('title');
            $table->text('description');
            $table->text('img_id');
            $table->text("type")->nullable();
            $table->text("modality")->nullable();
            $table->integer('price');
            $table->integer('rooms')->nullable();
            $table->integer('bathrooms')->nullable();
            $table->integer('covered_area')->nullable();
            $table->integer('total_area')->nullable();
            $table->boolean('plain_water')->default(false);
            $table->boolean('natural_gas')->default(false);
            $table->boolean('light')->default(false);
            $table->boolean('pavement')->default(false);
            $table->boolean('kitchen')->default(false);
            $table->boolean('living_room')->default(false);
            $table->boolean('playground')->default(false);
            $table->integer('antique')->nullable();
            $table->text("country")->nullable();
            $table->text("administrative_area_level_1")->nullable();
            $table->text("administrative_area_level_2")->nullable();
            $table->text("administrative_area_level_3")->nullable();
            $table->text("sublocality_level_1")->nullable();
            $table->text("sublocality_level_2")->nullable();
            $table->text("sublocality_level_3")->nullable();
            $table->text("locality")->nullable();
            $table->text("route")->nullable();
            $table->text("street_number")->nullable();
            $table->text("formatted_address")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('properties');
    }
}

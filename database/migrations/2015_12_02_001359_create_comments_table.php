<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments("id");
            $table->integer("sender_id")->nullable();
            $table->text("sender_name")->nullable();
            $table->text("sender_lastname")->nullable();
            $table->text("sender_phone")->nullable();
            $table->text("sender_mail")->nullable();
            $table->integer("receiver_id");
            $table->text("subject")->nullable();
            $table->text("message");
            $table->dateTime("date_time");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comments');
    }
}

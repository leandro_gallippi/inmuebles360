<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->text("cuit")->nullable();
            $table->text("profile_img")->nullable();
            $table->integer("age")->nullable();
            $table->double("dni")->nullable();
            $table->text("phone")->nullable();
            $table->text("mobile_phone")->nullable();
            $table->text("lastname")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn("cuit");
            $table->dropColumn("profile_img");
            $table->dropColumn("age");
            $table->dropColumn("dni");
            $table->dropColumn("phone");
            $table->dropColumn("mobile_phone");
            $table->dropColumn("lastname");
        });
    }
}

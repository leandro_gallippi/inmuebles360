<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddModalityAndTypeToPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {    
	if (!Schema::hasColumn('properties', 'modality') && 
		!Schema::hasColumn('properties', 'type') ){
		   Schema::table('properties', function (Blueprint $table) {
	            $table->integer('modality');
        	    $table->integer('type');
        	});
	}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->dropColumn('modality');
            $table->dropColumn('type');
        });
    }
}

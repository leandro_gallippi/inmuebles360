<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPriceToPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('properties', 'price')) {
            Schema::table('properties', function (Blueprint $table) {
                $table->integer('price');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('properties', 'price')) {
            Schema::table('properties', function (Blueprint $table) {
                $table->dropColumn('price');
            });
        }
    }
}

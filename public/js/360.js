function render(){
	
	requestAnimationFrame(render);
	
	if(!manualControl){
		longitude += 0.1;
	}

	// limiting latitude from -85 to 85 (cannot point to the sky or under your feet)
	latitude = Math.max(-85, Math.min(85, latitude));

	// moving the camera according to current latitude (vertical movement) and longitude (horizontal movement)
	camera.target.x = 500 * Math.sin(THREE.Math.degToRad(90 - latitude)) * Math.cos(THREE.Math.degToRad(longitude));
	camera.target.y = 500 * Math.cos(THREE.Math.degToRad(90 - latitude));
	camera.target.z = 500 * Math.sin(THREE.Math.degToRad(90 - latitude)) * Math.sin(THREE.Math.degToRad(longitude));
	camera.lookAt(camera.target);

	// calling again render function
	renderer.render(scene, camera);
	
}

// when the mouse is pressed, we switch to manual control and save current coordinates
function onDocumentMouseDown(event){

	event.preventDefault();

	manualControl = true;

	savedX = event.clientX;
	savedY = event.clientY;

	savedLongitude = longitude;
	savedLatitude = latitude;

}

// when the mouse moves, if in manual contro we adjust coordinates
function onDocumentMouseMove(event){

	if(manualControl){
		longitude = (savedX - event.clientX) * 0.1 + savedLongitude;
		latitude = (event.clientY - savedY) * 0.1 + savedLatitude;
	}

}

// when the mouse is released, we turn manual control off
function onDocumentMouseUp(event){

	manualControl = false;

}

// pressing a key (actually releasing it) changes the texture map
document.onkeyup = function(event){

	panoramaNumber = (panoramaNumber + 1) % panoramasArray.length
	sphereMaterial.map = THREE.ImageUtils.loadTexture(panoramasArray[panoramaNumber])

}

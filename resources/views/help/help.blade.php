@extends("layouts.base")

@section("head")

  <script type="text/javascript">
    $(document).ready(function(data) {
      $("a[id^=pregunta], a[id^=respuesta]").click(function() {
        var question = $(this).attr('id');
        var numberOfQuestion = question.substr(question.length - 1);
        var realIndex;
        $("a[id^=pregunta]").each(function(index) {
          realIndex = index + 1;
          if (realIndex != numberOfQuestion) {
            $("#pregunta" + realIndex).removeClass("active");
            $("#respuesta" + realIndex).removeClass("active");
          }
          else {
            $("#pregunta" + realIndex).addClass("active");
            $("#respuesta" + realIndex).addClass("active");
          }
        });
      });
    });
  </script>
@endsection

@section("content")

<div class="container">
<div class="row row-head">
  <div class="col-md-12">
    <h2><strong>Ayuda</strong></h2>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
    <div class="panel panel-default">
      <div class="panel-heading">
        Preguntas
      </div>
      <div class="panel-body" style="padding: 0px;">
        <div class="list-group">
          <a id="pregunta1" href="#respuesta1" class="list-group-item active">
            <h4 class="list-group-item-heading">1. ¿Qué es Inmuebles 360?</h4>
          </a>
          <a id="pregunta2" href="#respuesta2" class="list-group-item">
            <h4 class="list-group-item-heading">2. ¿Cómo busco una propiedad?</h4>
          </a>
          <a id="pregunta3" href="#respuesta3" class="list-group-item">
            <h4 class="list-group-item-heading">3. ¿Quién puede registrarse?</h4>
          </a>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="list-group">
          <a id="respuesta1" href="#respuesta1" class="list-group-item active">
            <h4 class="list-group-item-heading">1. ¿Qué es Inmuebles 360?</h4>
            <p class="list-group-item-text">Inmuebles360 es un portal de compra y venta de inmuebles. La mayor variedad de casas, departamentos, oficinas comerciales y más. Busca tu próxima casa en ZonaProp, filtrando por Ciudad y Barrio. Recorre la propiedad en profundidad desde tu smartphone o desde la web, con nuestro exclusivo sistema de recorrido en 360 grados.</p>
          </a>
          <a id="respuesta2" href="#respuesta2" class="list-group-item">
            <h4 class="list-group-item-heading">2. ¿Cómo busco una propiedad?</h4>
            <p class="list-group-item-text">Para buscar una propiedad puedes utilizar el buscador en la página principal del sitio. Para eso, proporcionamos distintos filtros:
          <ul>
            <li>Tipo de propiedad: Casa, departamento, oficina...</li>
            <li>Zona</li>
            <li>Tipo de contrato: Compra, alquiler, emprendimiento...</li>
            <li>Rango de precio, fijando precio mínimo y máximo</li>
          </ul>
          Luego de iniciada la búsqueda puedes ordenar el listado de ofertas encontradas o cambiar la búsqueda en cualquier momento.</p>
          </a>
          <a id="respuesta3" href="#respuesta3" class="list-group-item">
            <h4 class="list-group-item-heading">3. ¿Quién puede registrarse?</h4>
            <p class="list-group-item-text">Para registrarse en Inmuebles360 se debe ser una empresa o persona particular mayor de 18 años, que tenga por objeto la intermediación o corretaje inmobiliario. No es necesario estar registrado para ver nuestro catálogo de propiedades.</p>
          </a>
        </div>
  </div>
</div>
</div>
@endsection
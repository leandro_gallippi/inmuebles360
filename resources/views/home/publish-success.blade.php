@extends("layouts.base") @section("content")

<div class="container">
<div id="title" class="alert alert-success text-center" role="alert">
    <!-- le pongo id=title para que matchee el del css -->
    <h1>Inmueble publicado.</h1>
    <a href="/propertyDetails/<?php echo $publicationId; ?>" class="btn btn-primary">Ver publicacion</a>
</div>
</div>
@endsection
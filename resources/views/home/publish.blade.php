@extends("layouts.base") @section("head")

<script type="text/javascript">
    $(document).ready(function () {
        $(document).keypress(
            function (event) {
                if (event.which == '13') {
                    event.preventDefault();
                }
            });
    });
</script>

@endsection @section("content")

<div class="container">
<section id="titleSection">
    <h1 id="title" align="center">Publicar Inmueble</h1>
</section>
<section id="form-publicacion">

    <form action="publish/store" method="POST" class="form-horizontal" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="inputTitulo" class="col-sm-2 control-label">Titulo</label>
            <div class="col-sm-8">
                <input type="text" name="titulo" class="form-control" id="inputTitulo" required>
            </div>
        </div>
        <div class="form-group">
            <label for="autocomplete" class="col-sm-2 control-label">Dirección</label>
            <div class="col-sm-8">
                <input class="form-control" id="autocomplete" placeholder="Ingrese una dirección" onBlur="geolocate()" type="text" value="{{Input::get(" formatted_address ")}}" required="true"></input>
            </div>
        </div>

        <div class="form-group">
            <label for="inputAntiguedad" class="col-sm-2 control-label">Antiguedad</label>
            <div class="col-sm-2">
                <div class="input-group">
                    <input type="number" name="antiguedad" class="form-control" id="inputAntiguedad">
                    <span class="input-group-addon">años</span>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="inputTipo" class="col-sm-2 control-label">Tipo de propiedad</label>
            <div class="col-sm-8">
                {!! HtmlHelper::selectFromDicc(PropertyTypeDefinitions::map(), 'id', 'name', Input::get("type"), ['class' => 'form-control', 'name' => 'type', 'required' => 'true'], false) !!}
            </div>
        </div>

        <div class="form-group">
            <label for="inputModalidad" class="col-sm-2 control-label">Modalidad</label>
            <div class="col-sm-8">
                {!! HtmlHelper::selectFromDicc(PropertyModalityDefinitions::map(), 'id', 'name', Input::get("modality"), ['class' => 'form-control', 'name' => 'modality', 'required' => 'true'], false) !!}
            </div>
        </div>

        <div class="form-group">
            <label for="inputPrecio" class="col-sm-2 control-label">Precio</label>
            <div class="col-sm-2">
                <div class="input-group">
                    <span class="input-group-addon">AR$</span>
                    <input type="number" name="precio" class="form-control" id="inputPrecio" required>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="inputAmbientes" class="col-sm-2 control-label">Ambientes</label>
            <div class="col-sm-1">
                <input type="number" name="ambientes" class="form-control" id="inputAmbientes">
            </div>
        </div>

        <div class="form-group">
            <label for="inputBanios" class="col-sm-2 control-label">Baños</label>
            <div class="col-sm-1">
                <input type="number" name="banios" class="form-control" id="inputBanios">
            </div>
        </div>

        <div class="form-group">
            <label for="inputSuperficieCub" class="col-sm-2 control-label">Superficie cubierta</label>
            <div class="col-sm-1">
                <input type="number" name="supCubierta" class="form-control" id="inputSuperficieCub">
            </div>
            <label for="inputSuperficieTotal" class="col-sm-2 control-label">Superficie total</label>
            <div class="col-sm-1">
                <input type="number" name="supTotal" class="form-control" id="inputSuperficieTotal">
            </div>
        </div>
       <!-- <div class="form-group">
            <label for="photoInput" class="col-sm-2 control-label">Fotos</label>
            <div class="col-sm-8">
                <input id="photoInput" required="required" name="photos[]" type="file" multiple class="file-loading">
                <script>
                    $("#photoInput").fileinput({
                        uploadUrl: "notNull",
                        uploadAsync: false,
                        browseLabel: "Seleccionar",
                        maxFileCount: 4,
                        dropZoneEnabled: false,
                        showUpload: false
                    });
                </script>
            </div>
        </div>-->

        <div class="form-group">
            <label for="photo360Input" class="col-sm-2 control-label">Fotos 360</label>
            <div class="col-sm-8">
                <input id="photo360Input" required="required" name="photos360[]" type="file" multiple class="file-loading">
                <script>
                    $("#photo360Input").fileinput({
                        uploadUrl: "notNull", // Necesita no ser null para que funcione el plugin berreta este
                        uploadAsync: false,
                        browseLabel: "Seleccionar",
                        maxFileCount: 4,
                        dropZoneEnabled: false,
                        showUpload: false
                    });
                </script>
                <p class="help-block">Recomendamos utilizar sphere para tomar las fotos 360.</p>
            </div>
        </div>

        <div class="form-group">
            <label for="inputOtros" class="col-sm-2 control-label">Otros</label>
            <div class="col-sm-8">
                <label class="checkbox-inline">
                    <input type="checkbox" name="gasNatural" id="checkGasNatural" value="true"> Gas natural
                </label>
                <label class="checkbox-inline">
                    <input type="checkbox" name="aguaCorriente" id="checkAguaCorriente" value="true"> Agua corriente
                </label>
                <label class="checkbox-inline">
                    <input type="checkbox" name="luz" id="checkLuz" value="true"> Luz
                </label>
                <label class="checkbox-inline">
                    <input type="checkbox" name="pavimentado" id="checkPavimentado" value="true"> Pavimentado
                </label>
                <label class="checkbox-inline">
                    <input type="checkbox" name="cocina" id="checkCocina" value="true"> Cocina
                </label>
                <label class="checkbox-inline">
                    <input type="checkbox" name="livingRoom" id="checkLivingRoom" value="true"> Living room
                </label>
                <label class="checkbox-inline">
                    <input type="checkbox" name="playground" id="checkPlayground" value="true"> Playground
                </label>
            </div>
        </div>

        <div class="form-group">
            <label for="inputDescripcion" class="col-sm-2 control-label">Descripcion</label>
            <div class="col-sm-8">
                <textarea class="form-control" name="desc" rows="3" id="inputDescripcion" required></textarea>
            </div>
        </div>
        <input type="hidden" name="formatted_address" value="{{Input::get('formatted_address')}}">
        <input type="hidden" name="country" value="{{Input::get('country')}}">
        <input type="hidden" name="locality" value="{{Input::get('locality')}}">
        <input type="hidden" name="administrative_area_level_1" value="{{Input::get('administrative_area_level_1')}}">
        <input type="hidden" name="administrative_area_level_2" value="{{Input::get('administrative_area_level_2')}}">
        <input type="hidden" name="administrative_area_level_3" value="{{Input::get('administrative_area_level_3')}}">
        <input type="hidden" name="sublocality_level_1" value="{{Input::get('ublocality_level_1')}}">
        <input type="hidden" name="sublocality_level_2" value="{{Input::get('sublocality_level_2')}}">
        <input type="hidden" name="sublocality_level_3" value="{{Input::get('sublocality_level_3')}}">
        <input type="hidden" name="route" value="{{Input::get('route')}}">
        <input type="hidden" name="street_number" value="{{Input::get('street_number')}}">
        <input type="hidden" name="postal_code" value="{{Input::get('postal_code')}}">
        <input type="hidden" name="latitude" value="{{Input::get('latitude')}}">
        <input type="hidden" name="longitude" value="{{Input::get('longitude')}}">

        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-8">
                <a href="{{ URL::previous() }}" class="btn btn-default">Cancelar</a>

                <button type="submit" class="col-sm-offset-5 btn btn-default">Publicar
                </button>

            </div>
        </div>
    </form>


</section>
</div>
@endsection @section("footer")

<script>
    // This example displays an address form, using the autocomplete feature
    // of the Google Places API to help users fill in the information.

    var placeSearch, autocomplete;
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        administrative_area_level_2: 'short_name',
        administrative_area_level_3: 'short_name',
        sublocality_level_1: 'short_name',
        sublocality_level_2: 'short_name',
        sublocality_level_3: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };

    function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */
            (document.getElementById('autocomplete')), {
                types: ['geocode']
            });

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
    }

    // [START region_fillform]
    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        console.log(place);
        for (var component in componentForm) {
            $("input[name=" + component + "]").val("");
        }
        $("input[name=formatted_address]").val("");

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        if (place.address_components) {
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    $("input[name=" + addressType + "]").val(val);
                }
            }
            $("input[name=formatted_address]").val(place.formatted_address);
            $("button#save").prop("disabled", false);
        } else {
            $("button#save").prop("disabled", true);
        }
    }
    // [END region_fillform]

    // [START region_geolocation]
    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
        var inputAddress = $('#autocomplete').val();

        var geocoder = new google.maps.Geocoder();
        var address = inputAddress;

        geocoder.geocode({
            'address': address
        }, function (results, status) {

            if (status == google.maps.GeocoderStatus.OK) {

                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                $("input[name=latitude]").val(latitude);
                $("input[name=longitude]").val(longitude);
            }
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?signed_in=true&libraries=places&callback=initAutocomplete" async defer></script>
@endsection
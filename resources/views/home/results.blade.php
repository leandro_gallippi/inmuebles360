@extends("layouts.base")

@section("head")

    <script type="text/javascript">
    $(document).ready(function(){
      $(document).keypress(
          function(event){
            if (event.which == '13') {
              event.preventDefault();
            }
      });
    });
  </script>

  <style type="text/css">
    .pagination {
      float: right !important;
    }
    @media (min-width: 1000px) {
      #propertyDescription {
        padding-left: 0px;
      }  
      #propertyImage {
        padding-right: 0px;
      }
    }
    

  </style>

@endsection

@section("content")
<div class="container">
    <div class="row row-head">
        <div class="col-md-12">
            <h2><strong>{{$presenter->countProperties()}} resultados</strong></h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="row">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        Busca inmuebles
                    </div>
                    <div class="panel-body">
                        <form method="get">
                            <div class="dropdown form-group text-center" id="selectType">
                                {!! HtmlHelper::selectFromDicc(PropertyTypeDefinitions::map(), 'id', 'name', Input::get("type"), ['class' => 'form-control', 'name' => 'type'], false) !!}
                            </div>
                            <div class="form-group text-center">
                                <input class="form-control" id="autocomplete" placeholder="Ingrese una dirección" onBlur="geolocate()" type="text" value="{{Input::get("formatted_address")}}"></input>
                            </div>
                            <div class="dropdown form-group text-center">
                                {!! HtmlHelper::selectFromDicc(PropertyModalityDefinitions::map(), 'id', 'name', Input::get("modality"), ['class' => 'form-control', 'name' => 'modality'], false) !!}
                            </div>
                            <div class="form-group text-center">
                                Precio desde <input type="number" class="form-control" name="min_price">
                            </div>
                            <div class="form-group text-center">
                                Precio hasta <input type="number" class="form-control" name="max_price">
                            </div>
                            <div class="form-group" style="margin-left:20%">
                                <h4>Servicios</h4>
                                <input type="checkbox" name="natural_gas" value="1" {{(Input::has('natural_gas')) ? 'checked' : ''}}> Gas natural<br/>
                                <input type="checkbox" name="plain_water" value="1" {{(Input::has('plain_water')) ? 'checked' : ''}}> Agua corriente<br/>
                                <input type="checkbox" name="light" value="1" {{(Input::has('light') == 1) ? 'checked' : ''}}> Luz<br/>
                                <input type="checkbox" name="pavement" value="1" {{(Input::has('pavement')) ? 'checked' : ''}}> Pavimentado<br/>
                                <input type="checkbox" name="kitchen" value="1" {{(Input::has('kitchen') == 1) ? 'checked' : ''}}> Cocina<br/>
                                <input type="checkbox" name="living_room" value="1" {{(Input::has('living_room')) ? 'checked' : ''}}> Living<br/>
                                <input type="checkbox" name="playground" value="1" {{(Input::has('playground')) ? 'checked' : ''}}> Patio<br/>
                            </div>
                            <input type="hidden" name="formatted_address" value="{{Input::get('formatted_address')}}">
                            <input type="hidden" name="country" value="{{Input::get('country')}}">
                            <input type="hidden" name="locality" value="{{Input::get('locality')}}">
                            <input type="hidden" name="administrative_area_level_1" value="{{Input::get('administrative_area_level_1')}}">
                            <input type="hidden" name="administrative_area_level_2" value="{{Input::get('administrative_area_level_2')}}">
                            <input type="hidden" name="administrative_area_level_3" value="{{Input::get('administrative_area_level_3')}}">
                            <input type="hidden" name="sublocality_level_1" value="{{Input::get('ublocality_level_1')}}">
                            <input type="hidden" name="sublocality_level_2" value="{{Input::get('sublocality_level_2')}}">
                            <input type="hidden" name="sublocality_level_3" value="{{Input::get('sublocality_level_3')}}">
                            <input type="hidden" name="route" value="{{Input::get('route')}}">
                            <input type="hidden" name="street_number" value="{{Input::get('street_number')}}">
                            <input type="hidden" name="postal_code" value="{{Input::get('postal_code')}}">
                            <input type="hidden" name="latitude" value="{{Input::get('latitude')}}">
                            <input type="hidden" name="longitude" value="{{Input::get('longitude')}}">
                            <div class="form-group">
                                <button  type="submit" class="btn btn-success pull-right">Buscar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
          <div class="row">
            <div class="col-md-10">
              {!! $presenter->getPropertiesPagination() !!}
            </div>
          </div>
          @foreach ($presenter->getProperties() as $item)
            <div class="row">
                <div class="col-md-6" id="propertyImage">
                    <iframe src="/property/{{$item->id}}" alt="propiedad" class="img-detallesPropiedad" style="height: 230px"></iframe>
                </div>
                <div class="col-md-6" id="propertyDescription">
                  <div class="well">
                    <div class="control-group">
                        <h3 class="control-label"><strong>{{ $item->title }}</strong></h3>
                    </div>
                    <br/>
                    <div class="control-group">
                        <span class="control-label"><strong>Ubicacion:</strong> {{ $item->locality }}</span>
                    </div>
                    <div class="control-group">
                        <span class="control-label"><strong>Precio:</strong> ${{ $item->price }}</span>
                    </div>
                    <div class="control-group">
                        <span class="control-label"><strong>Ambientes:</strong> {{ $item->rooms }} | {{ $item->total_area }} metros cuadrados</span>
                    </div>
                    <br/>
                    <div class="text-center">
                        <a href="propertyDetails/{{ $item->id }}" class="btn btn-info">Ver publicacion</a>
                    </div>
                  </div>
                </div>
            </div>
          @endforeach
          <div class="row">
            <div class="col-md-10">
              {!! $presenter->getPropertiesPagination() !!}
            </div>
          </div>
    </div>
    </div>
</div>
@endsection

@section("footer")

  <script>
    // This example displays an address form, using the autocomplete feature
    // of the Google Places API to help users fill in the information.

    var placeSearch, autocomplete;
    var componentForm = {
      street_number: 'short_name',
      route: 'long_name',
      locality: 'long_name',
      administrative_area_level_1: 'short_name',
      administrative_area_level_2: 'short_name',
      administrative_area_level_3: 'short_name',
      sublocality_level_1: 'short_name',
      sublocality_level_2: 'short_name',
      sublocality_level_3: 'short_name',
      country: 'long_name',
      postal_code: 'short_name'
    };

    function initAutocomplete() {
      // Create the autocomplete object, restricting the search to geographical
      // location types.
      autocomplete = new google.maps.places.Autocomplete(
          /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
          {types: ['geocode']});

      // When the user selects an address from the dropdown, populate the address
      // fields in the form.
      autocomplete.addListener('place_changed', fillInAddress);
    }

    // [START region_fillform]
    function fillInAddress() {
      // Get the place details from the autocomplete object.
      var place = autocomplete.getPlace();
      console.log(place);
      for (var component in componentForm) {
        $("input[name=" + component + "]").val("");
      }
      $("input[name=formatted_address]").val("");

      // Get each component of the address from the place details
      // and fill the corresponding field on the form.
      if(place.address_components) {
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            $("input[name=" + addressType + "]").val(val);
          }
        }
        $("input[name=formatted_address]").val(place.formatted_address);
        $("button#save").prop("disabled", false);
      }
      else {
        $("button#save").prop("disabled", true);
      }
    }
    // [END region_fillform]

    // [START region_geolocation]
    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
        var inputAddress = $('#autocomplete').val();
    
        var geocoder = new google.maps.Geocoder();
        var address = inputAddress;

        geocoder.geocode( { 'address': address}, function(results, status) {

        if (status == google.maps.GeocoderStatus.OK) {
                
            var latitude = results[0].geometry.location.lat();
            var longitude = results[0].geometry.location.lng();
            $("input[name=latitude]").val(latitude);
            $("input[name=longitude]").val(longitude);
            } 
        }); 
    }

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?signed_in=true&libraries=places&callback=initAutocomplete"
      async defer></script>
@endsection
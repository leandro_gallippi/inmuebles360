@extends("layouts.base") 

@section("content")

<div class="container">
<div class="row row-head">
  <div class="col-md-12">
    <h2><strong>Editar Perfil</strong></h2>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <form action="editProfile" method="POST" class="form-horizontal" role="form" enctype="multipart/form-data">
      {{ csrf_field() }}
      @if(!app()->make('currentUser')->isIndividualUser())
        <div class="form-group">
          <label class="control-label col-sm-2" for="nombre">Nombre</label>
          <div class="col-sm-8">
            <input type="nombre" class="form-control" name="name" value="{{ app()->make('currentUser')->name }}">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-2" for="telefono">Teléfono</label>
          <div class="col-sm-8">
            <input type="telefono" class="form-control" name="phone" value="{{ app()->make('currentUser')->phone }}">
          </div>
        </div>
      @endif  
      <div class="form-group">
        <label class="control-label col-sm-2" for="telefono">Teléfono Móvil</label>
        <div class="col-sm-8">
          <input type="telefono" class="form-control" name="mobile_phone" value="{{ app()->make('currentUser')->mobile_phone }}">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2" for="mail">Email</label>
        <div class="col-sm-8">
          <input type="mail" class="form-control" name="email" value="{{ app()->make('currentUser')->email }}">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2" for="pwd">Password:</label>
        <div class="col-sm-8">          
          <input type="password" class="form-control" name="password" value="{{app()->make('currentUser')->password}}">
        </div>
      </div>
      @if(!app()->make('currentUser')->isIndividualUser())
        <div class="form-group">
          <label class="control-label col-sm-2" for="cuit">CUIT</label>
          <div class="col-sm-8">
            <input type="dni" class="form-control" name="cuit" value="{{app()->make('currentUser')->cuit }}">
          </div>
        </div>
      @endif
      <div class="form-group">
        <div class="col-sm-10">
          <a class = "btn btn-default pull-right" href = "{{app()->make('urls')->getUrlMyAccount()}}" >Cancelar</a>
          <button type="submit" class="btn btn-primary pull-right" style="margin-right: 10px;">Editar</button>
        </div>
      </div>
    </form>
  </div>
</div>
</div>
@endsection
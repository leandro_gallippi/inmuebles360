@extends("layouts.base") @section("content")

<div class="container">
  <div class="row row-head">
    <h1><strong>Perfil</strong></h1>
  </div>
<section>
<div class="container">
<div class="table-responsive">
<table class="table">
  @if(!app()->make('currentUser')->isIndividualUser())
    <tr>
      <th >Nombre</th>
      <td>{{ app()->make('currentUser')->name }}</td>
    </tr>
    <tr>
      <th>CUIT</th>
      <td>
        {{ app()->make('currentUser')->cuit }}
      </td>
    </tr>
  @endif
  <tr>
    <th >Teléfono</th>
    <td>{{ app()->make('currentUser')->phone }}<br></td>
  </tr>
  <tr>
    <th >Teléfono móvil</th>
    <td>{{ app()->make('currentUser')->mobile_phone }}<br></td>
  </tr>
  <tr>
    <th >E-mail</th>
    <td>{{ app()->make('currentUser')->email }}</td>
  </tr>
</table>
</div>
</div>
<div class="container">   
  <a href="/profile/properties" type="button" class="btn btn-primary" style="margin-left: 10%">Mis publicaciones</a>
  <button onclick="location.href='/editProfile';" type="button" class="btn btn-primary" style="margin-left: 20%">Editar perfil</button>
</div>
</section>
</div>
@endsection
@extends("layouts.base") @section("content")

<div class="container">
<section id="titleSection">
    <h1 id="title" align="center">Mis publicaciones</h1>
</section>
<section>
  @foreach ($ownProperties as $item)
      <div>
          <div class="col-lg-3" style="display: inline-block">
              <iframe class="img-destacada" src="/property/{{$item->id}}" alt="propiedad"></iframe>
          </div>
          <div class="well result-view">
              <div class="control-group">
                  <h3 class="control-label">{{ $item->title }}</h3>
              </div>
              <br/>
              <div class="control-group">
                  <span class="control-label">Ubicacion: {{ $item->formatted_address }}</span>
              </div>
              <div class="control-group">
                  <span class="control-label">Precio: $ {{ $item->price }}</span>
              </div>
              <div class="control-group">
                  <span class="control-label">Ambientes: {{ $item->rooms }} | {{ $item->total_area }} metros cuadrados</span>
              </div>
              <br/>
              <div class="text-center">
                  <button type="button" class="btn btn-info">
                      <a href="/propertyDetails/{{ $item->id }}">Ver publicacion </a>
                  </button>
              </div>
          </div>
      </div>
  @endforeach
</section>
</div>
@endsection
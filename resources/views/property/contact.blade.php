@extends("layouts.base")


@section("content")
<div class="container">
	<div class="row row-head">
		<div class="col-md-12">
			<h2><strong>Contactarse con {{$vendedor->name}}</strong></h2>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<form class="form-horizontal">
				<div class="form-group">
					<label class="col-sm-2 control-label">Nombre</label>
					<div class="col-sm-8">
						<input class="form-control" type="text" name="name"></input>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Email</label>
					<div class="col-sm-8">
						<input class="form-control" type="text" name="email"></input>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Telefono</label>
					<div class="col-sm-8">
						<input class="form-control" type="text" name="tel"></input>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Mensaje</label>
					<div class="col-sm-8">
						<textarea class="form-control" name="message"></textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-10">
						<button type="submit" class="btn btn-primary pull-right">Enviar</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection


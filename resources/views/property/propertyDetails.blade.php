@extends("layouts.base")

@section("content")

<div class="container">
	<div class="row row-head">
	  <div class="col-md-12">
	    <h2><strong></strong>{{ $property->title }}</h2>
	  </div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-body" style="padding: 0">
					<iframe class="img-detallesPropiedad center-block" src="/property/{{$property->getFirstPhoto()}}" alt="propiedad"></iframe>
				</div>
				<div class="panel-footer">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<span class="control-label dimgray"><strong>Precio:</strong> ${{ $property->price }}</span>
						    </div>
						    <div class="form-group">
								<span class="control-label dimgray"><strong>Nombre:</strong> {{ ($property->user) ? $property->user->name : "-" }}</span>
						    </div>
							<div class="form-group">
								<span class="control-label dimgray"><strong>Pais:</strong> {{ $property->country }}</span>
						    </div>
							<div class="form-group">
								<span class="control-label dimgray"><strong>Localidad:</strong> {{ $property->locality }}</span>
						    </div>
							<div class="form-group">
								<span class="control-label dimgray"><strong>Calle:</strong> {{ $property->formatted_address }}</span>
						    </div>
							<div class="form-group">
								<span class="control-label dimgray"><strong>Tipo:</strong> {{ PropertyTypeDefinitions::getName($property->type, '-') }}</span>
						    </div>
                            <div class="form-group">
								<span class="control-label dimgray"><strong>Antiguedad:</strong> {{ $property->antique? $property->antique . ' Años' : '-' }}</span>
						    </div>
						</div>
						<div class="col-md-6">
                            <div class="form-group">
								<span class="control-label dimgray"><strong>Modalidad:</strong> {{ $property->modality? $property->modality : '-' }}</span>
						    </div>
							<div class="form-group">
								<span class="control-label dimgray"><strong>Ambientes:</strong> {{ $property->rooms? $property->rooms : '-' }}</span>
						    </div>
							<div class="form-group">
								<span class="control-label dimgray"><strong>Baños:</strong> {{ $property->bathrooms? $property->bathrooms: '-' }}</span>
						    </div>
							<div class="form-group">
								<span class="control-label dimgray"><strong>Area total:</strong> {{ $property->total_area? $property->total_area : '-' }}</span>
						    </div>
							<div class="form-group">
								<span class="control-label dimgray"><strong>Otros:</strong>
                                    {{ $property->plain_water? 'Agua corriente' : '' }}
                                    {{ $property->natural_gas? 'Gas natural' : '' }}
                                    {{ $property->light? 'Luz' : '' }}
                                    {{ $property->pavement? 'Pavimentado' : '' }}
                                    {{ $property->kitchen? 'Cocina' : '' }}
                                    {{ $property->living_room? 'Living' : '' }}
                                    {{ $property->playground? 'Playground' : '' }}
                                </span>
						    </div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group text-center">
						    	<a href="/contact/{{$property->user_id}}" class="btn btn-info">Contactar Vendedor</a>	
						    </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			@foreach ($property->photos as $photo)
	            <div class="publicacion-destacada col-lg-3">
	                <p>
			            <iframe class="img-destacada center-block" src="/property/{{$photo->id}}" alt="propiedad"></iframe>
	                </p>
	            </div>
	        @endforeach
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-success">
				<div class="panel-heading">
					Preguntas
				</div>
				@if(!($property->comments->isEmpty()))
					<div class="panel-body">
						<ul class="list-group">
						@foreach ($property->comments as $comentario)
							<li class="list-group-item">
								({{ $comentario->date_time }}): <strong>{{ $comentario->message}}</strong>
							</li>
				         @endforeach
						</ul>
					</div>
				@endif
				<div class="panel-footer">
					<form action="comment" method="POST" class="form-horizontal" enctype="multipart/form-data">
				        {{ csrf_field() }}
				        <input type="hidden" name="property_id" value="{{$property->id}}">
				        <div class="form-group">
				        	<textarea name="message" value="{{Input::get('message')}}" placeholder="Detalle" class="form-control"></textarea>
				        </div>
				        <div class="form-group text-center">
				        	<button type="submit" class="btn btn-warning">Preguntar</button>
				        </div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection


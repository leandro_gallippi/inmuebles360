<html>
	<head>
		<style>
			body{
				margin: 0;
			}
			canvas{
				width: 100%;
				height: 100%
			}
		</style>
		<script src="//cdnjs.cloudflare.com/ajax/libs/three.js/r69/three.min.js"></script>
		<script type="text/javascript" src="/js/360.js"></script>
		
	</head>
	<body>
		<div>
			<script>
				var manualControl = false;
				var longitude = 0;
				var latitude = 0;
				var savedX;
				var savedY;
				var savedLongitude;
				var savedLatitude;
				var width = window.innerWidth;
				var height = window.innerHeight;

				// panoramas background
				//var panoramasArray = ["/img/02.jpg"];
				//var panoramaNumber = Math.floor(Math.random() * panoramasArray.length);
				var pathImagen = "/img/{{$nombreImagen}}";


				// setting up the renderer
				renderer = new THREE.WebGLRenderer();
				renderer.setSize(width, height);
				document.body.appendChild(renderer.domElement);

				// creating a new scene
				var scene = new THREE.Scene();

				// adding a camera
				var camera = new THREE.PerspectiveCamera(75, (width) / (height), 1, 100);
				camera.target = new THREE.Vector3(0, 0, 0);

				// creation of a big sphere geometry
				var sphere = new THREE.SphereGeometry(100, 100, 40);
				sphere.applyMatrix(new THREE.Matrix4().makeScale(-1, 1, 1));

				// creation of the sphere material
				var sphereMaterial = new THREE.MeshBasicMaterial();
				sphereMaterial.map = THREE.ImageUtils.loadTexture(pathImagen)

				// geometry + material = mesh (actual object)
				var sphereMesh = new THREE.Mesh(sphere, sphereMaterial);
				scene.add(sphereMesh);

				// listeners
				document.addEventListener("mousedown", onDocumentMouseDown, false);
				document.addEventListener("mousemove", onDocumentMouseMove, false);
				document.addEventListener("mouseup", onDocumentMouseUp, false);	
				render();	
			</script>
		</div>
	</body>
</html>
